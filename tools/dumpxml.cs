using System;
using System.Xml;

public class Test {

	public static void Main2(string[] args) {
		XmlDocument doc = new XmlDocument();
		if (true) {
			XmlValidatingReader reader = new XmlValidatingReader(new XmlTextReader(Console.In));
			reader.ValidationType = ValidationType.None;
			doc.Load(reader);
		} else {
			doc.Load(new XmlTextReader(Console.In));
		}
		DumpDoc(doc.DocumentElement, 0);
	}

	public static void Main(string[] args) {
		XmlValidatingReader reader = new XmlValidatingReader(new XmlTextReader(Console.In));
		reader.ValidationType = ValidationType.None;
		while (reader.Read()) {
			Console.WriteLine(reader.NodeType + " " + reader.Name + " " + reader.NamespaceURI);
		}
	}

	static void DumpDoc(XmlElement elem, int indent) {
		for (int i = 0; i < indent; i++) Console.Write("  ");
		Console.WriteLine(elem.Name + " " + elem.NamespaceURI);
		foreach (XmlElement e in elem.SelectNodes("*"))
			DumpDoc(e, indent+1);
	}
}
